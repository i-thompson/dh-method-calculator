public class Matrix_Handler {

    /**
     * Generic class to handle matrix calculations
     */
    public Matrix_Handler(){

    }

    /**
     * Multiply together two matrices, string-wise
     * Simplifies terms in input and output
     * @param m1 left hand matrix
     * @param m2 right hand matrix
     * @return m1m2
     */
    public Matrix multiplyMatrices(Matrix m1, Matrix m2){

        String[][] result = new String[4][4];

        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                String product = "";
                for(int v = 0; v < 4; v++){
                    String term1 = m1.matrix[i][v];
                    String term2 = m2.matrix[v][j];

                    // clean inputs for common trig values
                    term1 = banishTrig(term1);
                    term2 = banishTrig(term2);

                    if(term1.contains("+")){
                        term1 = "(".concat(term1).concat(")");
                    }
                    if(term2.contains("+")){
                        term2 = "(".concat(term2).concat(")");
                    }


                    if(!(term1.equals("0") || term2.equals("0") )){
                        if(term1.equals("1")){
                            product = product.concat(term2);
                        }
                        else if(term2.equals("1") ){
                            product = product.concat(term1);
                        }
                        else{
                            if(term1.startsWith("-") && term2.startsWith("-")){
                                if(term1.equals("-1") && term2.equals("-1")){
                                    product = product.concat("1");
                                }
                                else{
                                    product = product.concat(term1.substring(1).concat(term2.substring(1)));
                                }
                            }
                            else if(term2.startsWith("-")){
                                product = product.concat("-".concat(term1).concat(term2.substring(1)));
                            }
                            else{
                                product = product.concat(term1.concat(term2));
                            }
                        }
                        if(v != 3){
                            product = product.concat("+");
                        }
                    }
                }
                result[i][j] = cleanOutput(product);
            }
        }
        return new Matrix(result);
    }

    /**
     * Simplify term by removing a value which equals 1
     * @param term term containing a value equal to 1
     * @return simplified term
     */
    public String removeSubstring(String term, String bitToRemove){
        if(term.equals(bitToRemove)){
            term = "1";
        }
        else{
            int index = term.indexOf(bitToRemove);
            String start = term.substring(0, index);
            String end = "";
            if(index != term.length() - bitToRemove.length()){
                end = term.substring(index + bitToRemove.length());
            }

            term = start.concat(end);
        }
        if(term.equals("")) term = "1";
        return term;
    }

    /**
     * Simplify common trig terms for sin and cos
     * @param term term which contains a trig value
     * @return simplified version of the value
     */
    public String banishTrig(String term){

        if(term.contains("cos(0)")){
            term = removeSubstring(term,"cos(0)");
        }

        if(term.contains("sin(pi/2)")){
            term = removeSubstring(term,"sin(pi/2)");
        }
        if(term.contains("sin(-pi/2)")){
            term = "-".concat(removeSubstring(term,"sin(-pi/2)"));
        }

        if(term.contains("cos(pi)")){
            term = "-".concat(removeSubstring(term,"cos(pi)"));
        }

        if(term.contains("cos(-pi)")){
            term = "-".concat(removeSubstring(term,"cos(-pi)"));
        }

        if(term.contains("sin(0)") || term.contains("cos(pi/2)")|| term.contains("cos(-pi/2)") || term.contains("sin(pi)") || term.contains("sin(-pi)")){
            term = "0";
        }

        if(term.startsWith("--")){
            term = term.substring(2);
        }



        return term;
    }

    /**
     * Simplify output values for matrices
     * Performs basic simplifications, eg +-, --, trimming + from end of string, and removes common trig values
     * @param s matrix term to simplify
     * @return simplified version of input
     */
    public String cleanOutput(String s){
        // trim " + " from string
        if(s.endsWith("+")){
            s = s.substring(0, s.length() - 1);
        }

        // simplify +- to -
        for(int i = 0; i < s.length()-3; i++){
            if(s.substring(i, i+2).equals("+-") || s.substring(i, i+2).equals("-+")){
                s = s.substring(0, i).concat("-".concat(s.substring(i+2)));
            }
        }

        s = banishTrig(s);

        if(s.equals("")) s="0";

        return s;
    }
}
