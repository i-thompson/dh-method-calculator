# DH Method Calculator

## Name
Calcuator for the Devanit-Hartenberg method from parameters.


## Description
This project aims to speed up and reduce error likelihood with transforming the DH method parameters into Homogeneous Transformation Matrices for forward kinematics. It allows a single matrix to be gererated from parameters, and a matrix multiplication to be performed from the paramaters of both matrices.

Please note that the notation used in this version of the DH method differs from literature as it follows the method used by my University.


## Usage

The program requires the user to decide the axes and model parameters. It essentially takes the DH-table and produces matrices for the forward kinematics between subsequent joints.

### Deciding Model Parameters

The axes can be decided as follows:

| Axis | Description |
| ---- | ----------- |
| Z    | Along joint axis. For a rotary joint this points into the joint, and for a prismatic joint this is in the direction of motion. |
| X    | Perpendicular to the current and previous Z axes. Aim to make this point along the joint. |
| Y    | Use the right-hand rule on the X (thumb) and Z (middle finger) axes. |


The model parameters must be non-null string values. For example, theta1 is an acceptable value. The parameters can be decided manually as follows:
| Parameter | Description |
| --------- | ----------- |
| Theta     | Clockwise rotation about the Z-axis |
| Alpha     | Clockwise rotation about the X-axis to get from previous Z to current Z |
| a         | Distance between the Z axes in the X direction |
| d         | Distance between the X axes in the Z direction |

Values 0, +-pi, and +-pi/2 are all simplified if provided as values for alpha and theta since they simplify to whole numbers.

### Use of the Program

When run from the command line, enter an integer value to represent the action you want as prompted.
These are:
1. Generate a new matrix
2. Multiply matrices (clean), ie show only the result
3. Multiply matrices (verbose), ie show the working

DONE: Computation finished

The program then prompts the user to input the various values to perform this computation.

## Roadmap
Since this project was created to reduce manual computation for an open-book exam, further work is unlikely to be completed. 

Some suggestions for further improvements include:
- [ ] Simplify trig
- [ ] Rearrange equations

## Authors and acknowledgment
Authored by Imogen

