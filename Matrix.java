public class Matrix {

    String[][] matrix; // 2D array representation of the matrix

    /**
     * Sets the matrix as the T-Matrix from these values
     * @param theta theta for the joint
     * @param alpha alpha for the joint
     * @param a a for the joint
     * @param d d for the joint
     */
    public Matrix(String theta, String alpha, String a, String d){
        matrix = generate_T_Matrix(theta, alpha, a, d);
        Matrix_Handler handler = new Matrix_Handler();
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                matrix[i][j] = handler.banishTrig(matrix[i][j]);
            }
        }
    }

    /**
     * Sets the matrix based off a given 2D array
     * @param seededMatrix 2D array representation of the matrix
     */
    public Matrix(String[][] seededMatrix){
        matrix = seededMatrix;
    }

    /**
     * fills the matrix with the input params to make it a T-matrix
     * does not simplify the terms
     * @param theta theta for the matrix
     * @param alpha alpha for the matrix
     * @param a a for the matrix
     * @param d d for the matrix
     */
    public String[][] generate_T_Matrix(String theta, String alpha, String a, String d){

        String[] cleanVals = cleanVals(new String[]{a, d});
        String cleana = cleanVals[0];
        String cleand = cleanVals[1];

        String[][] T_matrix = new String[4][4];
        T_matrix[0][0] = String.format("cos(%s)", theta);
        T_matrix[0][1] = String.format("-sin(%s)cos(%s)", theta, alpha);
        T_matrix[0][2] = String.format("sin(%s)sin(%s)", theta, alpha);
        T_matrix[0][3] = String.format("%scos(%s)", cleana, theta);

        T_matrix[1][0] = String.format("sin(%s)", theta);
        T_matrix[1][1] = String.format("cos(%s)cos(%s)", theta, alpha);
        T_matrix[1][2] = String.format("-cos(%s)sin(%s)", theta, alpha);
        T_matrix[1][3] = String.format("%ssin(%s)", cleana, theta);

        T_matrix[2][0] = String.format("0");
        T_matrix[2][1] = String.format("sin(%s)", alpha);
        T_matrix[2][2] = String.format("cos(%s)", alpha);
        T_matrix[2][3] = String.format("%s", cleand);

        T_matrix[3][0] = String.format("0");
        T_matrix[3][1] = String.format("0");
        T_matrix[3][2] = String.format("0");
        T_matrix[3][3] = String.format("1");

        Matrix_Handler h = new Matrix_Handler();
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                T_matrix[i][j] = h.banishTrig(T_matrix[i][j]);
                if(T_matrix[i][j].contains("0") && !T_matrix[i][j].contains("+")){
                    T_matrix[i][j] = "0";
                }
            }
        }


        return T_matrix;
    }

    /**
     * Brackets a and d if appropriate
     * @param vals array containing a and d
     * @return array containing a and d, with brackets if necessary
     */
    public String[] cleanVals(String[] vals){
        for(int i = 0; i < 2; i++){
            if(vals[i].contains("+")){
                vals[i] = "(".concat(vals[i]).concat(")");
            }
        }
        return vals;
    }


    /**
     * Print the matrix values, aligned by column
     */
    public void printMatrix(){
        int[] colWidths = new int[4];
        for(String[] row: matrix){
            for(int c = 0; c < 4; c++){
                int width = String.valueOf(row[c]).length();
                colWidths[c] = Math.max(colWidths[c], width);
            }
        }

        for(String[] row: matrix){
            for(int c = 0; c < 4; c++){
                String fmt = String.format("%s%%%ss%s",
                        c == 0 ? " " : "  ",
                        colWidths[c],
                        c < 4 - 1 ? " " : "%n");
                System.out.printf(fmt, row[c]);
            }
        }
    }

}
