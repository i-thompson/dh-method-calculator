import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);


        while (true){

            System.out.println("Please select the action you want:");
            System.out.println("DONE: Computation finished");
            System.out.println("1: Generate a new matrix");
            System.out.println("2: Multiply matrices (clean)");
            System.out.println("3: Multiply matrices (verbose)");


            String in = s.next();

            switch (in){
                case "1":
                    Matrix m = genMatrixFromInput();
                    System.out.println("New matrix generated:");
                    m.printMatrix();
                    break;
                case "2":
                    System.out.println("Generate left hand matrix");
                    Matrix m1 = genMatrixFromInput();
                    System.out.println("Generate right hand matrix");
                    Matrix m2 = genMatrixFromInput();

                    Matrix_Handler h = new Matrix_Handler();

                    Matrix m3 = h.multiplyMatrices(m1, m2);

                    m3.printMatrix();
                    break;
                case "3":
                    System.out.println("--------------------------");
                    System.out.println("Generate left hand matrix");
                    Matrix ma = genMatrixFromInput();
                    System.out.printf("---%s---\n", "Left matrix");
                    ma.printMatrix();
                    System.out.println("--------");
                    System.out.println("Generate right hand matrix");
                    Matrix mb = genMatrixFromInput();
                    System.out.printf("---%s---\n", "Right matrix");
                    mb.printMatrix();
                    System.out.println("--------");

                    h = new Matrix_Handler();

                    Matrix mc = h.multiplyMatrices(ma, mb);
                    System.out.printf("---%s---\n", "Multiplied matrix");
                    mc.printMatrix();
                    System.out.println("--------");
                    System.out.println("--------------------------");
                    break;
                case "DONE":
                    return;

            }

        }




    }

    private static Matrix generateMatrixFromValues(String theta, String alpha, String a, String d) {
        Matrix m = new Matrix(theta, alpha, a, d);
        return m;
    }

    private static Matrix genMatrixFromInput(){
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Theta: ");
        String theta = scanner.next();
        System.out.printf("Alpha: ");
        String alpha = scanner.next();
        System.out.printf("a: ");
        String a = scanner.next();
        System.out.printf("d: ");
        String d = scanner.next();

        return generateMatrixFromValues(theta, alpha, a, d);
    }



}
